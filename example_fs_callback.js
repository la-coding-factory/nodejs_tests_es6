/* Lib d'exemple, utilisant 'fs' et les callbacks */
import fs from 'fs'

function lireFichier(path) {
	return fs.readFile(path, { encoding: 'utf-8' }, function(err, data) {
		if (err)
			return console.error(err.message)
		console.log(data)
		return data;
	})
}

/* Que se passe-t-il si on appelle la fonction lireFichier ? */
const test_a = lireFichier('path') // que contient test_a ?
