/* Lib d'exemple, utilisant 'fs' et les PROMESSES (Promise) */
import fs from 'fs/promises'

function lireFichier(path) {
	return fs.readFile(path, { encoding: 'utf-8' })
		.then(data => {
			console.log(data)
			return data;
		})
		.catch(err => {
			console.log(err)
			return err
		})
}

/* Que se passe-t-il si on appelle la fonction lireFichier ? */
const test_b = lireFichier('path') // que contient test_b ?
