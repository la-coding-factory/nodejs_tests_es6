

function testAsync() {
	return new Promise(function(resolve, reject) {
		resolve(1)
	})
}

/* Explicitement asynchrone */
async function bis_testAsync() {
	return 1
}

testAsync()
	.then(function(data) {
		console.log(data)
	})
	.catch(err => {
		console.error(err)
	})

// const resultAsync = testAsync()
// console.log(resultAsync)

// const bis_resultAsync = bis_testAsync()
// console.log(bis_resultAsync)
