import { EventEmitter } from 'events'

class EventManager extends EventEmitter {}

const manager = new EventManager()

manager.on('test-event', (data) => {
	console.log(data)
})

manager.on('test-end', () => {
	console.log('end')
})

manager.emit('test-event', { n: 42 })
manager.emit('test-event', { b: 42 })
manager.emit('test-event', { c: 42 })
manager.emit('test-end')
