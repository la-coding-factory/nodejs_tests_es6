# Repo de test pour la syntaxe ES6 avec NodeJS

# Qu'est-ce que "ES" 6

ES pour EcmaScript, ce sont les fondements de JavaScript qui n'est qu'un "nom de scene"

6 pour la version 2015
7 version 2016
...


## ES6 - La nouvelle syntaxe de javascript

### Prérequis

Ajouter une ligne dans votre `package.json`
```json
{
  "type": "module"
}
```

> Le type par défaut est "commonjs" (le JS commun)

## Spécifications

Le système d'importation de fichier et lib est maintenant différent

### Export globaux
> maLib.js

ES6:
```js
const SOMETHING = {} // function, objet, tableau, variable..
export default SOMETHING
```

Correspond en ES5 à:
```js
module.exports = SOMETHINIG
```

### Export nommés

ES6:
```js
const SOMETHING = {} // function, objet, tableau, variable..
export function laFunc() {}
export const NAME = "VALUE"
export SOMETHING
export {
	a,
	b,
	c
}
```

Correspond en ES5 à:
```js
module.exports.laFunc = function laFunc() {}
module.exports.NAME = NAME
module.exports.SOMETHING = SOMETHING
module.exports.a = a
module.exports.b = b
```

### Comment importer

ES6 - globaux
```js
import maLib from './maLib.js'
```

ES6 - nommés
```js
import { laFunc } from './maLib.js'
```

ES5 - globaux
```js
const maLib = require('./maLib.js')
```

ES5 - globaux
```js
const { laFunc } = require('./maLib.js')
```

# Les callbacks

Doc : https://developer.mozilla.org/fr/docs/Glossary/Callback_function

```js
function callbackFunc(err, data) {
	if (err) {
		console.error('Une erreur est survenue :' + err.message)
	}
	else {
		console.log(data)
	}
}

readFile('path', callbackFunc)
```
