import { createReadStream } from 'fs'
import csv from 'csv-parser'


function createResults() {
	const results = []

	return new Promise((resolve, reject) => {
		createReadStream('euro.csv', { encoding: 'utf8' })
			.pipe(csv({ separator: ';' }))
			.on('data', data => {
				results.push(data)

			})
			.on('end', () => {
				resolve(results)
			})
			.on('error', reject)
	})
}

createResults()
	.then(results => {
		// stats ...
	})

